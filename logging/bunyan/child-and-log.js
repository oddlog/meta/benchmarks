"use strict";

const Benchmark = require("benchmark");
const bunyan = require("bunyan");
const DummyWriteStream = require("../_util/DummyWriteStream");

const fakeStream0 = new DummyWriteStream();
const fakeStream1 = new DummyWriteStream();

const log0 = bunyan.createLogger({name: "some-id", streams: []});
const log1 = bunyan.createLogger({name: "some-id", streams: [{stream: fakeStream0}]});
const log2 = bunyan.createLogger({name: "some-id", streams: [{stream: fakeStream0}, {stream: fakeStream1}]});

const suite = new Benchmark.Suite("bunyan/child-and-log");

const obj0 = {hello: "world"}, obj1 = {lorem: "ipsum", dolor: "sit", amet: obj0, truth: 42, date: new Date()};

suite
    .add("bunyan:child-and-log:0T-0P:", () => log0.child().info("hello world"))
    .add("bunyan:child-and-log:0T-1P:", () => log0.child(obj0).info("hello world"))
    .add("bunyan:child-and-log:0T-5P:", () => log0.child(obj1).info("hello world"))
    .add("bunyan:child-and-log:1T-0P:", () => log1.child().info("hello world"))
    .add("bunyan:child-and-log:1T-1P:", () => log1.child(obj0).info("hello world"))
    .add("bunyan:child-and-log:1T-5P:", () => log1.child(obj1).info("hello world"))
    .add("bunyan:child-and-log:2T-0P:", () => log2.child().info("hello world"))
    .add("bunyan:child-and-log:2T-1P:", () => log2.child(obj0).info("hello world"))
    .add("bunyan:child-and-log:2T-5P:", () => log2.child(obj1).info("hello world"))
    // add listeners
    .on("error", err => console.error(err))
    .on("cycle", event => console.log(String(event.target)))
    // run async
    .run({async: true});
