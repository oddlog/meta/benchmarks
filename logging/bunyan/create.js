"use strict";

const Benchmark = require("benchmark");
const bunyan = require("bunyan");

const suite = new Benchmark.Suite("bunyan/create");

suite
    .add("bunyan:create:0T:", () => bunyan.createLogger({name: "some-id", streams: []}))
    .add("bunyan:create:1T:", () => bunyan.createLogger({
      name: "some-id", streams: [{type: "stream", stream: process.stdout}]
    }))
    // add listeners
    .on("error", err => console.error(err))
    .on("cycle", event => console.log(String(event.target)))
    // run async
    .run({async: true});
