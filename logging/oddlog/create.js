"use strict";

const Benchmark = require("benchmark");
const oddlog = require("oddlog");

const suite = new Benchmark.Suite("oddlog/create");

suite
    .add("oddlog:create:0T:", () => {
      return oddlog.createLogger("some-id", {transports: null});
    })
    .add("oddlog:create:1T:", () => {
      return oddlog.createLogger("some-id", {transports: [{type: "stream", stream: process.stdout}]});
    })
    // add listeners
    .on("error", err => console.error(err))
    .on("cycle", event => console.log(String(event.target)))
    // run async
    .run({async: true});
