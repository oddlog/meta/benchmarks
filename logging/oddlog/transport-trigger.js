"use strict";

const _ = require("lodash");
const Benchmark = require("benchmark");
const oddlog = require("oddlog");
const transportTrigger = require("@oddlog/transport-trigger");
const DummyWriteStream = require("../_util/DummyWriteStream");

oddlog.mixin(transportTrigger.plugin);

const suite = new Benchmark.Suite("oddlog/message-plain");

const fakeStream = new DummyWriteStream();

const logSilentObj = {
  transports: [{
    type: "trigger",
    immediateOwnership: false, // in real-world applications most logging commands should transfer ownership
    transports: [{stream: fakeStream}]
  }]
};
const logPrintedObj = {
  transports: [{
    type: "trigger",
    immediateOwnership: false, // in real-world applications most logging commands should transfer ownership
    // it's probably enabled in real-world applications, but for performance testing we consider it a legit disable:
    guard: false,
    transports: [{stream: fakeStream}]
  }]
};

const obj0 = {hello: "world"}, obj1 = {lorem: "ipsum", dolor: "sit", amet: obj0, truth: 42, date: new Date()};

const logSilent = oddlog.createLogger("some-id", logSilentObj);
const logPrinted = oddlog.createLogger("some-id", logPrintedObj);

const cachePrinted = logPrinted.transports[0]._cache;

logSilent.info("initial message");
for (let i = 0; i < cachePrinted.limit; i++) { logPrinted.info("hello world"); }

suite
    .add("oddlog:transport-trigger:silent-0P:", () => logSilent.info("hello world"))
    .add("oddlog:transport-trigger:silent-1P:", () => logSilent.info(obj0, "hello world"))
    .add("oddlog:transport-trigger:silent-5P:", () => logSilent.info(obj1, "hello world"))
    .add("oddlog:transport-trigger:printed-0P:", () => logPrinted.error("hello world"))
    .add("oddlog:transport-trigger:printed-1P:", () => logPrinted.error(obj0, "hello world"))
    .add("oddlog:transport-trigger:printed-5P:", () => logPrinted.error(obj1, "hello world"))
    // add listeners
    .on("error", err => console.error(err))
    .on("cycle", event => console.log(String(event.target)))
    // run async
    .run({async: true});
