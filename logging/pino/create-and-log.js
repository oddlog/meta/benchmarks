"use strict";

const Benchmark = require("benchmark");
const DummyWriteStream = require("../_util/DummyWriteStream");
const pino = require("pino");

const suite = new Benchmark.Suite("pino/create-and-message");

const fakeStream = new DummyWriteStream();

const log0Obj = {name: "some-id", safe: false, timestamp: true, enabled: false};
const log1Obj = {name: "some-id", safe: false, timestamp: true};
const obj0 = {hello: "world"}, obj1 = {lorem: "ipsum", dolor: "sit", amet: obj0, truth: 42, date: new Date()};

suite
    .add("pino:create-and-log:printed-0T-0P:", () => log0().info("hello world"))
    .add("pino:create-and-log:printed-0T-1P:", () => log0().info(obj0, "hello world"))
    .add("pino:create-and-log:printed-0T-5P:", () => log0().info(obj1, "hello world"))
    .add("pino:create-and-log:printed-1T-0P:", () => log1().info("hello world"))
    .add("pino:create-and-log:printed-1T-1P:", () => log1().info(obj0, "hello world"))
    .add("pino:create-and-log:printed-1T-5P:", () => log1().info(obj1, "hello world"))
    // .add("pino:create-and-log:silent-0T-0P:", () => log0().trace("hello world"))
    // .add("pino:create-and-log:silent-0T-1P:", () => log0().trace(obj0, "hello world"))
    // .add("pino:create-and-log:silent-0T-5P:", () => log0().trace(obj1, "hello world"))
    .add("pino:create-and-log:silent-1T-0P:", () => log1().trace("hello world"))
    .add("pino:create-and-log:silent-1T-1P:", () => log1().trace(obj0, "hello world"))
    .add("pino:create-and-log:silent-1T-5P:", () => log1().trace(obj1, "hello world"))
    // add listeners
    .on("error", err => console.error(err))
    .on("cycle", event => console.log(String(event.target)))
    // run async
    .run({async: true});

function log0() { return pino(log0Obj); }

function log1() { return pino(log1Obj, fakeStream); }
