"use strict";

const Benchmark = require("benchmark");
const pino = require("pino");

const suite = new Benchmark.Suite("pino/create");

suite
    .add("pino:create:0T:", () => pino({name: "some-id", safe: false, timestamp: true, enabled: false}))
    .add("pino:create:1T:", () => pino({name: "some-id", safe: false, timestamp: true}))
    // add listeners
    .on("error", err => console.error(err))
    .on("cycle", event => console.log(String(event.target)))
    // run async
    .run({async: true});
