"use strict";

const _ = require("lodash");
const Benchmark = require("benchmark");
const DummyWriteStream = require("../_util/DummyWriteStream");
const pino = require("pino");

const suite = new Benchmark.Suite("pino/message-merge");

const fakeStream = new DummyWriteStream();

const log0Obj = {name: "some-id", safe: false, timestamp: true, enabled: false};
const log1Obj = {name: "some-id", safe: false, timestamp: true};
const obj0 = {hello: "world"}, obj1 = {lorem: "ipsum", dolor: "sit", amet: obj0, truth: 42, date: new Date()};
const pl0 = {got: "you"}, pl1 = {lorem: "dolor", some: "value", commit: "me", obj0, see: "ya"};

const log00 = pino(_.assign(log0Obj, pl0));
const log01 = pino(_.assign(log0Obj, pl1));
const log10 = pino(_.assign(log1Obj, pl0), fakeStream);
const log11 = pino(_.assign(log1Obj, pl1), fakeStream);

// Since the initial message might differ in performance (at least within oddlog it does given certain conditions)...
log10.info("initial message");
log11.info("initial message");

suite
    .add("pino:message-merge:printed-0T-1P-0P:", () => log00.info("hello world"))
    .add("pino:message-merge:printed-0T-1P-1P:", () => log00.info(obj0, "hello world"))
    .add("pino:message-merge:printed-0T-1P-5P:", () => log00.info(obj1, "hello world"))
    .add("pino:message-merge:printed-0T-5P-0P:", () => log01.info("hello world"))
    .add("pino:message-merge:printed-0T-5P-1P:", () => log01.info(obj0, "hello world"))
    .add("pino:message-merge:printed-0T-5P-5P:", () => log01.info(obj1, "hello world"))
    .add("pino:message-merge:printed-1T-1P-0P:", () => log10.info("hello world"))
    .add("pino:message-merge:printed-1T-1P-1P:", () => log10.info(obj0, "hello world"))
    .add("pino:message-merge:printed-1T-1P-5P:", () => log10.info(obj1, "hello world"))
    .add("pino:message-merge:printed-1T-5P-0P:", () => log11.info("hello world"))
    .add("pino:message-merge:printed-1T-5P-1P:", () => log11.info(obj0, "hello world"))
    .add("pino:message-merge:printed-1T-5P-5P:", () => log11.info(obj1, "hello world"))
    // .add("pino:message-merge:silent-0T-1P-0P:", () => log00.trace("hello world"))
    // .add("pino:message-merge:silent-0T-1P-1P:", () => log00.trace(obj0, "hello world"))
    // .add("pino:message-merge:silent-0T-1P-5P:", () => log00.trace(obj1, "hello world"))
    // .add("pino:message-merge:silent-0T-5P-0P:", () => log01.trace("hello world"))
    // .add("pino:message-merge:silent-0T-5P-1P:", () => log01.trace(obj0, "hello world"))
    // .add("pino:message-merge:silent-0T-5P-5P:", () => log01.trace(obj1, "hello world"))
    .add("pino:message-merge:silent-1T-1P-0P:", () => log10.trace("hello world"))
    .add("pino:message-merge:silent-1T-1P-1P:", () => log10.trace(obj0, "hello world"))
    .add("pino:message-merge:silent-1T-1P-5P:", () => log10.trace(obj1, "hello world"))
    .add("pino:message-merge:silent-1T-5P-0P:", () => log11.trace("hello world"))
    .add("pino:message-merge:silent-1T-5P-1P:", () => log11.trace(obj0, "hello world"))
    .add("pino:message-merge:silent-1T-5P-5P:", () => log11.trace(obj1, "hello world"))
    // add listeners
    .on("error", err => console.error(err))
    .on("cycle", event => console.log(String(event.target)))
    // run async
    .run({async: true});
