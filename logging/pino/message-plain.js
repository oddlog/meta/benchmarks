"use strict";

const Benchmark = require("benchmark");
const DummyWriteStream = require("../_util/DummyWriteStream");
const pino = require("pino");

const suite = new Benchmark.Suite("pino/message-plain");

const fakeStream = new DummyWriteStream();

const log0Obj = {name: "some-id", safe: false, timestamp: true, enabled: false};
const log1Obj = {name: "some-id", safe: false, timestamp: true};
const obj0 = {hello: "world"}, obj1 = {lorem: "ipsum", dolor: "sit", amet: obj0, truth: 42, date: new Date()};

const log0 = pino(log0Obj);
const log1 = pino(log1Obj, fakeStream);

// Since the initial message might differ in performance (at least within oddlog it does given certain conditions)...
log1.info("initial message");

suite
    .add("pino:message-plain:printed-0T-0P:", () => log0.info("hello world"))
    .add("pino:message-plain:printed-0T-1P:", () => log0.info(obj0, "hello world"))
    .add("pino:message-plain:printed-0T-5P:", () => log0.info(obj1, "hello world"))
    .add("pino:message-plain:printed-1T-0P:", () => log1.info("hello world"))
    .add("pino:message-plain:printed-1T-1P:", () => log1.info(obj0, "hello world"))
    .add("pino:message-plain:printed-1T-5P:", () => log1.info(obj1, "hello world"))
    // .add("pino:message-plain:silent-0T-0P:", () => log0.trace("hello world"))
    // .add("pino:message-plain:silent-0T-1P:", () => log0.trace(obj0, "hello world"))
    // .add("pino:message-plain:silent-0T-5P:", () => log0.trace(obj1, "hello world"))
    .add("pino:message-plain:silent-1T-0P:", () => log1.trace("hello world"))
    .add("pino:message-plain:silent-1T-1P:", () => log1.trace(obj0, "hello world"))
    .add("pino:message-plain:silent-1T-5P:", () => log1.trace(obj1, "hello world"))
    // add listeners
    .on("error", err => console.error(err))
    .on("cycle", event => console.log(String(event.target)))
    // run async
    .run({async: true});
