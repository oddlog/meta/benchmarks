"use strict";

const Benchmark = require("benchmark");

const suiteOddLog = new Benchmark.Suite("array-clone-small");

let arraySmall = new Array(50);
let arrayMid = new Array(500);
let arrayLarge = new Array(5000);

suiteOddLog
  .add("a.Fill-null", () => arraySmall.fill(null))
  .add("a.Fill-void", () => arraySmall.fill())
  .add("a.new      ", () => new Array(arraySmall.length))
  .add("a.Map      ", () => arraySmall.map(noop))
  .add("b.Fill-null", () => arrayMid.fill(null))
  .add("b.Fill-void", () => arrayMid.fill())
  .add("b.new      ", () => new Array(arrayMid.length))
  .add("b.Map      ", () => arrayMid.map(noop))
  .add("c.Fill-null", () => arrayLarge.fill(null))
  .add("c.Fill-void", () => arrayLarge.fill())
  .add("c.new      ", () => new Array(arrayLarge.length))
  .add("c.Map      ", () => arrayLarge.map(noop))
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

function noop() { }

/*
node@10.7.0

  a)
    21M new
    9.0M Fill-void
    8.8M Fill-null
    6.5M Map
  b)
    2.4M new
    1.3M Fill-null
    1.2M Fill-void
    1.0M Map
  c)
    253K new
    133K Fill-null
    130K Fill-void
     46K Map
*/

/*
Usages
- CacheScope (cleanUp)
 */
