"use strict";

const Benchmark = require("benchmark");

const suiteOddLog = new Benchmark.Suite("array-clone-small");

let array = [];

for (let i = 0; i < 5; i++) { array.push(i); }

suiteOddLog
  .add("Iterate", () => {
    let clone = [];
    let _len = array.length;
    for (let i = 0; i < _len; i++) { clone.push(mapFn(array[i])); }
  })
  .add("Map    ", () => array.map(mapFn))
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

function mapFn(value) { return value + 1; }

/*
node@10.7.0

  57.7M Map
  33.8M Iterate
*/
