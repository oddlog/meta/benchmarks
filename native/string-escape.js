/* eslint max-len: "off" */
"use strict";

const Benchmark = require("benchmark");

const suiteOddLog = new Benchmark.Suite("json");

let msg0 = "application startup";
let msg1 = "application \"startup\"";

const regex = /([\\"])/g, replaceValue = "\\$1";
suiteOddLog
  .add("a.Custom    ", () => "\"" + fastEscape(msg0) + "\"")
  .add("a.Stringify ", () => JSON.stringify(msg0))
  .add("a.Regex     ", () => msg0.replace(regex, replaceValue))

  .add("b.Custom    ", () => "\"" + fastEscape(msg1) + "\"")
  .add("b.Stringify ", () => JSON.stringify(msg1))
  .add("b.Regex     ", () => msg1.replace(regex, replaceValue))
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

/*
node@10.7.0

  a)
    20.0M Custom
     4.6M Stringify
     3.2M Regex
  b)
    6.7M Custom
    4.5M Stringify
    2.1M Regex
*/

function fastEscape(str) {
  let result = "";
  let someEscape = false;
  let lastIdx = 0;
  let _len = str.length;
  for (let i = 0; i < _len; i++) {
    const code = str.charCodeAt(i);
    if (code === 34 || code === 92) {
      result += str.slice(lastIdx, i) + "\\";
      lastIdx = i;
      someEscape = true;
    } else if (code < 32) {
      result += str.slice(lastIdx, i);
      lastIdx = i + 1;
    }
  }
  return someEscape ? result + str.slice(lastIdx) : str;
}
