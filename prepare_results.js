/* eslint-disable no-sync,no-empty */
"use strict";

const fs = require("fs");
const os = require("os");
const path = require("path");
const util = require("util");
const readline = require("readline");

const oddlogVersion = require("oddlog/package.json").version;
const bunyanVersion = require("bunyan/package.json").version;
const pinoVersion = require("pino/package.json").version;

const RESULTS_IN_FILE = path.join(__dirname, ".results");

const RESULTS_INFO_FILE = path.join(__dirname, "results", Date.now() + ".info");
const RESULTS_TXT_FILE = path.join(__dirname, "results", Date.now() + ".txt");
const RESULTS_CSV_FILE = path.join(__dirname, "results", Date.now() + ".csv");

/*==================================================== Execution  ====================================================*/

const rl = readline.createInterface({input: fs.createReadStream(RESULTS_IN_FILE)});

const resultsMap = {};

rl.on("line", parseLine);
rl.on("close", () => {
  unlinkIfExists(RESULTS_INFO_FILE);
  unlinkIfExists(RESULTS_TXT_FILE);
  unlinkIfExists(RESULTS_CSV_FILE);
  writeMeta(fs.createWriteStream(RESULTS_INFO_FILE));
  writeHuman(fs.createWriteStream(RESULTS_TXT_FILE));
  writeCSV(fs.createWriteStream(RESULTS_CSV_FILE));
});

/*==================================================== Functions  ====================================================*/

function unlinkIfExists(file) {
  try {
    fs.accessSync(file, fs.R_OK);
    fs.unlinkSync(file);
  } catch (ignored) {}
}

function parseLine(line) {
  const split = line.split(":");
  const [logger, suite, test, results] = split;
  const opsString = /^[0-9,]+/.exec(results.substring(3))[0];
  const opsPerSecond = +(opsString.replace(/,/g, ""));
  const suffix = results.substr(opsString.length + 3);
  resultsMap[suite] = resultsMap[suite] || {};
  resultsMap[suite][test] = resultsMap[suite][test] || [];
  resultsMap[suite][test].push({logger, opsPerSecond, suffix});
}

function writeMeta(stream) {
  stream.write("Library versions: ");
  stream.write(util.inspect({oddlog: oddlogVersion, bunyan: bunyanVersion, pino: pinoVersion}, {compact: false}));
  stream.write(os.EOL);
  stream.write(os.EOL);
  stream.write("Process versions: ");
  const processVersions = {
    node: process.versions.node, v8: process.versions.v8, modules: process.versions.modules,
    unicode: process.versions.unicode, zlib: process.versions.zlib
  };
  stream.write(util.inspect(processVersions, {compact: false}) + os.EOL);
  stream.write(os.EOL);
  stream.write("System: " + os.platform() + " " + os.release() + " " + os.arch() + os.EOL);
  stream.write(os.EOL);
  stream.write("CPUs:" + os.EOL);
  stream.write("  " + os.cpus().map((cpu) => cpu.model).join(os.EOL + "  "));
  stream.write(os.EOL);
}

function writeCSV(stream) {
  stream.write(["suite", "test", "logger", "ops/sec", "\"relative error margin\"", "samples"].join("\t") + os.EOL);
  for (let suite in resultsMap) {
    // noinspection JSUnfilteredForInLoop
    for (let test in resultsMap[suite]) {
      // noinspection JSUnfilteredForInLoop
      const resultsArray = resultsMap[suite][test];
      resultsArray.sort((a, b) => b.opsPerSecond - a.opsPerSecond);
      for (let result of resultsArray) {
        const errorMargin = /±([\d.]+)%/.exec(result.suffix)[1];
        const samples = /\((\d+)/.exec(result.suffix)[1];
        // noinspection JSUnfilteredForInLoop
        const data = [suite, test, result.logger, result.opsPerSecond, errorMargin, samples];
        stream.write(data.join("\t") + os.EOL);
      }
    }
  }
}

function writeHuman(stream) {
  let first = true;
  const resultsKeys = Object.keys(resultsMap).sort();
  for (let suite of resultsKeys) {
    if (!first) { stream.write(os.EOL); }
    first = false;
    stream.write(suite + ":" + os.EOL);
    // noinspection JSUnfilteredForInLoop
    for (let test in resultsMap[suite]) {
      stream.write("  " + test + ":" + os.EOL);
      // noinspection JSUnfilteredForInLoop
      stream.write(loggerResultHuman(resultsMap[suite][test], "    "));
    }
  }
}

function loggerResultHuman(resultsArray, linePrefix) {
  let string = "";
  resultsArray.sort((a, b) => b.opsPerSecond - a.opsPerSecond);
  let best = resultsArray[0].opsPerSecond;
  let first = true;
  for (let result of resultsArray) {
    string += linePrefix;
    string += pad(result.logger, 6, " ", true) + ": " + pad(withDelim(result.opsPerSecond), 11, " ") + result.suffix;
    if (!first) { string += " - factor " + floatingDigits(best / result.opsPerSecond, 2); }
    first = false;
    string += os.EOL;
  }
  return string;
}

function floatingDigits(num, count) {
  let factor = Math.pow(10, count);
  return ((num * factor) | 0) / factor;
}

function withDelim(num) {
  let numString = num + "";
  for (let i = numString.length - 3; i > 0; i -= 3) {
    numString = numString.substr(0, i) + "," + numString.substr(i);
  }
  return numString;
}

function pad(string, num, char, right) {
  const pad = num - string.length;
  if (pad <= 0) { return string; }
  let padding = char;
  while (padding.length < pad) { padding += char; }
  return right ? string + padding : padding + string;
}
